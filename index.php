<?php
class pickshablons {

    public function common($params) {
        
        if (!isset($_SESSION['user']['template'])) $_SESSION['user']['template'] = getTemplate();
        
        $dir = opendir(ROOT . '/template');
        $shablons = array();
        while ($file = readdir($dir)) {
            if (strpos($file, '.') === false) {
                $shablons[] = array(
                    'name' => $file,
                    'url' => '?shablon=' . $file,
                    'scrshot' => get_url('/template/' . $file . '/screenshot.png'),
                    'active' => ($_SESSION['user']['template'] == $file) ? 'active' : ''
                );
            }
        }
        closedir($dir);
    
        if (isset($_GET['shablon'])) {
            $template = $_GET['shablon'];
            $flag = False;
            foreach($shablons as $value) {
                if ($value['name'] == $template) {
                    $flag = True;
                    break;
                }
            }
            
            if ($flag) {
                $_SESSION['user']['template'] = $template;
            } else {
                redirect('/error.php?ac=404');
            }
        }
        
        $disign = '';
        $config = json_decode(file_get_contents(dirname(__FILE__).'/config.json'), true);
        
        if ($config['changingmenu']) {
            $disign = file_get_contents(dirname(__FILE__).'/template/index.html');
            
            $Viewer = new Viewer_Manager;
            $markers = array();
            $markers['tmp_list'] = array_slice($shablons, 0);
            $markers['plugin_path'] = '{{ plugin_path }}';
            
            $disign = $Viewer->parseTemplate($disign, array('data' => $markers));
        }
        $disign .= '</body>';
        return str_replace('</body>', $disign, $params);
    }
}
